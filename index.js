function inicio(){
    contenido = document.getElementById('contenido-archivo');
    pre = document.getElementById('logger');
    descargarp = document.getElementById('descargarp');
    descargarc = document.getElementById('descargarc');
    var boton = document.getElementById('file-input');
    boton.addEventListener('change',leerArchivo,false);
}
function leerArchivo(e) {
    var archivos = e.target.files;
    var archivo = archivos[0];
    if (!archivo) {
      return;
    }
    var lector = new FileReader();
    lector.readAsText(archivo);
    lector.addEventListener('load',mostrarContenido, false)
}
function mostrarContenido(e) {
    var resultado = e.target.result;
    contenido.innerHTML = resultado;
    var palabras = resultado.split(" ");
    const stack =new Stack();
    const queue =new Queue();
    for (var i = 0; i < palabras.length; i++) {
        if(palabras[i].toString().length<6){
            stack.push(palabras[i]);
        }else{
            queue.enqueue(palabras[i]);
        }
     }
    var tamp =stack.size();
    var contp = "PILA DE PALABRAS CON MENOS DE 6 CARACTERES";
    var contc = "COLA DE PALABRAS CON MÁS DE 5 CARACTERES";
     for (var i = 0; i < tamp; i++) {
        //Si se desea imprimir sirve el console log
        //console.log(stack.pop());
        //Si se desea grabar en txt se concatena
        contp+='\n'+stack.pop();
     }
     var tamc=queue.size();
    for (var i = 0; i <tamc; i++) {
        //Si se desea imprimir sirve el console log
        //console.log(queue.dequeue());
        //Si se desea grabar en txt se concatena
        contc+='\n'+queue.dequeue();
    }
    TxtPila(contp);
    TxtCola(contc);
}
window.addEventListener('load',inicio,false);
class Stack {
    constructor() {
        this.stack = [];
    }
    push(element) {
        this.stack.push(element);
        return this.stack;
    }
    
    pop() {
        return this.stack.pop();
    }
    
    peek() {
        return this.stack[this.stack.length - 1];
    }
    
    size() {
        return this.stack.length;
    }

    print() {
        console.log(this.stack);
    }
}
class Queue {
    constructor() {
        this.queue = [];
    }

    enqueue(element) {
        this.queue.push(element);
        return this.queue;
    }

    dequeue() {
        return this.queue.shift();
    }

    peek() {
        return this.queue[0];
    }

    size() {
        return this.queue.length;
    }

    isEmpty() {
        return this.queue.length === 0;
    }

    print() {
        console.log(this.queue);
    }
    }
function TxtPila(e)
{
    var contenidoDeArchivo = e;
var elem = document.getElementById('descargarp');

elem.download = "Pila.txt";
elem.href = "data:application/octet-stream," 
                     + encodeURIComponent(contenidoDeArchivo);
}
function TxtCola(e)
{
    var contenidoDeArchivo = e;
var elem = document.getElementById('descargarc');

elem.download = "Cola.txt";
elem.href = "data:application/octet-stream," 
                     + encodeURIComponent(contenidoDeArchivo);
}